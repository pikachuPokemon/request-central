Template.trending.onCreated(function(){
	this.subscribe('trending',Session.get('duration'));
});

Template.trending.helpers({
	noRequests: function(){
		return Requests.find().count()===0;
	},
	request: function(){
		return Requests.find({},{$sort:{votes:-1}});
	},
	fromNow: function(){
		return moment(this.addedAt).fromNow();
	}
});

Template.trending.events({
	'click .viewRequest':function(){
		Session.set('request',this._id);
		$('#requestModalOpen').click();
	}
});