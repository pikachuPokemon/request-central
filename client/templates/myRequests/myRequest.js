Template.myRequests.onCreated(function(){
	this.subscribe('trending',Session.get('duration'));
});

Template.myRequests.helpers({
	noRequests: function(){
		return Requests.find().count()===0;
	},
	request: function(){
		return Requests.find({userId: Meteor.userId()},{$sort:{votes:-1}});
	},
	fromNow: function(){
		return moment(this.addedAt).fromNow();
	}
});

Template.myRequests.events({
	'click .viewRequest':function(){
		Session.set('request',this._id);
		$('#requestModalOpen').click();
	}
});