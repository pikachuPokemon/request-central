Template.requestModal.helpers({
	request: function(){
		return Requests.findOne({_id: Session.get('request')});
	},
	demand:function(){
		var demand=Demand.findOne(Session.get('request'));
		var total =0;
		for(var property in demand){
			total+=demand[property];
		}
		Session.set('total',total);
		return demand;
	},
	comment: function(){
		return Comments.find({requestId: Session.get('request')});
	},
	avail : function(){
		return this.available?'available':'unavailable';
	},
	admin: function(){
		return Session.get('admin');
	}
});

Template.requestModal.events({
	'click #done':function(){
		$('*[data-dismiss="modal"]').click();
	},
	'click .like': function(){
		if(!Meteor.user()){
			sAlert.warning('You need to sign in first. Swipe left');
		}else{
			Meteor.call('like',this._id,function(error){
				if(error)
					sAlert.warning('Already upvoted');
				else{
					sAlert.success('upvoted!');
				}
			});
		}
	},
	'click .mark': function(){
		Meteor.call('mark',Session.get('request'),function(error){
			if(!error){
				sAlert.success('product marked available!');
			}
		});
	},
	'click #submitComment':function(event,templ){
		event.preventDefault();
		var content= $('#comment').val();
		var requestId=Session.get('request');
		Meteor.call('comment',requestId,content,function(error){
			if(!error){
				sAlert.success('Comment added');
				$('#comment').val(' ');
			}
		});
	}
});

Template.requestModal.onCreated(function(){
	this.subscribe('demand',Session.get('request'));
});