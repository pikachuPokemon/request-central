Template.layout.helpers({
	getBarStyle: function(){
		var currentRoute= Router.current().route.getName();
		if(currentRoute)
		if(currentRoute.indexOf('request')=== -1){
			return 'bar-calm';
		}else{
			return 'bar-energized';
		}
	},

	state: function(){
		return Session.get('admin')?'(true)':'(false)';
	}
/*	faceTaggingOutput: function(){
		return Router.current().route.getName()==='faceTaggingOutput';
	},
	home: function(){
		return !Router.current().route.getName();
	}*/
});