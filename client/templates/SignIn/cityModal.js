Template.cityModal.events({
	'click #setCity': function(){
		var city=$('#city').val();
		console.log(city);

		Meteor.call('setCity',city,function(){
			$('*[data-dismiss="modal"]').click();
			history.back();
			setTimeout(function(){
				sAlert.success('Welcome');
			},200);
		});
	}
});

Template.cityModal.helpers({
	city: function(){
		return App.cities;
	}
});