var onLogin= function(){
	setTimeout(function(){
		var user= Meteor.user();
		if(!user.city){
			$('#citySet').click();
		}else{
			history.back();
			setTimeout(function(){
				sAlert.success('Welcome');
			},200);
			
		}
	},200);

};

Template.signIn.events({
	'click #loginFacebook':function(){
		Meteor.loginWithFacebook({},function(error){
			if(!error)
				onLogin();
		});
	},
	'click #loginTwitter':function(){
		Meteor.loginWithTwitter({},function(error){
			if(!error)
				onLogin();
		});
	},
	'click #loginGoogle':function(){
		Meteor.loginWithGoogle({},function(error){
			if(!error)
				onLogin();
		});
	},

});