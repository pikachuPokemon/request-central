Template.request.helpers({
	category: function(){
		return App.categories;
	}
});

var getPicture = function(opts) {
   MeteorCamera.getPicture(opts, function(err, data) {
     if (err) {
       console.log('error', err);
     }
     if (data) {
       Session.set('image', data);
     }
   });
 };

Template.request.onCreated(function(){
	Session.set('image',false);
});
Template.request.events({
	'click #submit': function(event, templ){
		event.preventDefault();
		var request= {
			name: $('#name').val().trim(),
			description: $('#description').val().trim(),
			category: $('#category').val()
		};

		if(!request.name){
			sAlert.warning('Need name of product');
			return;
		}
		if(!request.description){
			sAlert.warning('Please fill in a brief description');
			return;
		}
		if(!request.category){
			sAlert.warning('Fill in category of product');
			return;
		}
		console.log(request);
		var picture=$('#picture').val();
		if(picture){
			request.picture= picture;
		}else{
			if(!!Session.get('image'))
				request.picture=Session.get('image');				
		}

		request.url=$('#url').val();
		if(!request.picture)
			sAlert.warning('Please Give a url or upload a picture');
		else{
			Meteor.call('createRequest',request,function(error){
				if(!error){
					Router.go('/');
					setTimeout(function(){
						sAlert.success('Request added!');
					},100);
					
				}
			});
		}
	},
	'click #upload': function(event){
		event.preventDefault();
		  if (Meteor.isCordova) {
		    getPicture({
		/*    width: 350,
		      height: 350,*/
		      quality: 80,
		      sourceType: Camera.PictureSourceType.PHOTOLIBRARY
		    });
		  } else {
		    alert('upload currently works on mobile only');
		  }
	}
});

Template.request.helpers({
	'image': function(){
		return !!Session.get('image');
	}
});