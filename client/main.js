Meteor.startup(function(){
	sAlert.config({
	    effect: 'flip',
	    position: 'bottom',
	    timeout: 3000	,
	    html: true,
	    onRouteClose: true,
	    stack: true,
	    offset: 0
	});

});

App= {
	cities: [
		'Delhi',
		'Mumbai',
		'Kolkata',
		'Bangaore',
		'Chennai'
	],

	categories: [
		'Electronics',
		'Gadget',
		'Appliance',
		'Healthcare',
		'Toys',
		'Sports',
		'Clothing and Accessories'
	]
};

login=function(){
	Router.go('signIn');
	$('*[data-ion-menu-close]').click();
};

logout= function(){
	Meteor.logout();
	Router.go('/');
	$('*[data-ion-menu-close]').click();
	sAlert.info('Good bye..');
};

Tracker.autorun(function(){
	if(Meteor.user()){
		Meteor.subscribe('user');
	}
});

toggleAdmin=function(){
	if(Session.get('admin'))
		Session.set('admin',false);
	else
		Session.set('admin',true);
};