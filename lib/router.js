Router.configure({
  layoutTemplate: 'layout'
});

var loggedIn= function(){
	if(Meteor.user()){
		this.next();
	}else{
		
		history.pushState({}, '', '/request');
		setTimeout(function(){
			sAlert.info('You need to sign in first');
		},200);
		Router.go('signIn');

	
	}
};

Router.route('/',function(){
	this.render('home');
});

Router.route('/request',function(){
	this.render('request');
});

Router.route('/signIn',function(){
	this.render('signIn')
});

Router.onBeforeAction(loggedIn, {
  only: ['request']
  // or except: ['routeOne', 'routeTwo']
});

Router.route('/myRequests',function(){
	this.render('myRequests');
})
Router.route('/myCoupons',function(){
	this.render('myCoupons');
})