App.info({
  name: 'Request Central',
  description: 'Request for upcoming/unavailable products and get coupons!',
  author: 'pikachuPokemon',
  version: '0.0.1'
});


App.setPreference('StatusBarOverlaysWebView', true);
App.setPreference('StatusBarStyle', 'default');
