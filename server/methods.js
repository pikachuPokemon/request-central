App= {
	cities: [
		'Delhi',
		'Mumbai',
		'Kolkata',
		'Bangaore',
		'Chennai'
	],

	categories: [
		'Gadget',
		'Appliance',
		'Healthcare',
		'Electronics',
		'Toys',
		'Sports',
		'Clothing and Accessories'
	],
	code: [
		'N9TT-9G0A-B7FQ-RANC',
		'QK6A-JI6S-7ETR-0A6C',
		'SXFP-CHYK-ONI6-S89U',
		'XNSS-HSJW-3NGU-8XTJ',
		'NHLE-L6MI-4GE4-ETEV',
		'6ETI-UIL2-9WAX-XHYO',
		'2E62-E3SR-33FI-XHV3',
		'7EIQ-72IU-2YNV-3L4Y'
	]
};

Meteor.methods({
	'setCity': function(city){
		Meteor.users.update({_id: Meteor.userId()},{$set: {city: city}});
	},
	'createRequest': function(request){
		request.votes=1;
		request.userId= Meteor.userId();
		request.available=false;
		request.availableUrl= '';
		request.userProfile=Meteor.users.findOne(Meteor.userId()).profile;
		request.addedAt= new Date();
		request.upvoters=[];
		var id=Requests.insert(request);

		var demand={};
		App.cities.forEach(function(city){
			demand[city]=0;
		});
		demand._id =id;
		Demand.insert(demand);


	},
	like: function(requestId){
		//else
			Requests.update({_id:requestId},{$push:{upvoters: Meteor.userId()},
											$inc: {votes:1}});

		var user=Meteor.user();
		var demand= Demand.findOne(requestId);
		demand[user.city]++;
		Demand.update(requestId,{$set: demand});


	},

	comment: function(requestId, content){
		var user= Meteor.user();
		var comment= {
			requestId: requestId,
			content: content,
			date: new Date(),
			userProfile: user.profile
		};
		Comments.insert(comment);
		console.log(comment);
	},
	mark :function(requestId){
		console.log(requestId);
		Requests.update(requestId,{$set: {available: true}});
			
		var request= Requests.findOne(requestId);
		Coupons.insert({
			name: request.name,
			picture: request.picture,
			userId: request.userId,
			code: App.code[Math.floor(Math.random()*(App.code).length)]
		});

	}
});