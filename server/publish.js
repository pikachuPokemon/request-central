Meteor.publish('user',function(){
	return Meteor.users.find({_id: this.userId});
});

Meteor.publish('trending',function(duration){
	return Requests.find();
	
});

Meteor.publish('demand',function(requestId){
	return [Demand.find({_id: requestId}), Comments.find({requestId: requestId})];
});

Meteor.publish('coupon',function(){
	return Coupons.find({userId: this.userId});
});